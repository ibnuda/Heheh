package me.ijauradunbi.heheh.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import me.ijauradunbi.heheh.model.Ask
import me.ijauradunbi.heheh.model.Comment
import me.ijauradunbi.heheh.model.Job
import me.ijauradunbi.heheh.model.Story

class CategoryPagerAdapter(fragmentManager: FragmentManager, val categoryIndex: Int)
: FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence {
        when (position) {
            0 -> return Story.NAME
            1 -> return Ask.NAME
            2 -> return Job.NAME
            3 -> return Comment.NAME
            else -> return ""
        }
    }
}