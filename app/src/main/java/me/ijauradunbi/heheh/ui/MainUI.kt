package me.ijauradunbi.heheh.ui

import android.app.Activity
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import me.ijauradunbi.heheh.R
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.support.v4.viewPager
import org.jetbrains.anko.verticalLayout

class MainUI : AnkoComponent<Activity> {
    override fun createView(ui: AnkoContext<Activity>): View {
        return with(ui) {
            verticalLayout {
                id = R.id.id_appbar
            }
            appBarLayout {
                backgroundColor = ContextCompat.getColor(ctx, R.color.material_grey_850)
                toolbar {
                    id = R.id.id_toolbar
                    setTitleTextColor(Color.WHITE)
                }

                tabLayout {
                    id = R.id.id_tab_layout
                }
            }

            viewPager {
                id = R.id.id_container
                backgroundColor = Color.BLACK
            }
        }
    }

}
