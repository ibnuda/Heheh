package me.ijauradunbi.heheh.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import org.jetbrains.anko.AnkoLogger

class HNItemFragment : Fragment(), AnkoLogger {
    companion object {
        val ARG_CATEGORY: String = "category"
        val ARG_POST_NO: String = "post_no"
        fun newInstance(category: String, postNo: Int): HNItemFragment {
            val arguments: Bundle = Bundle()
            arguments.putString(ARG_CATEGORY, category)
            arguments.putInt(ARG_POST_NO, postNo)
            val hnItemFragment: HNItemFragment = HNItemFragment()
            hnItemFragment.arguments = arguments
            return hnItemFragment
        }
    }


}
