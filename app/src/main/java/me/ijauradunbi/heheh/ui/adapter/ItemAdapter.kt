package me.ijauradunbi.heheh.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.ijauradunbi.heheh.R
import me.ijauradunbi.heheh.model.Story
import org.jetbrains.anko.find

class ItemAdapter(val context: Context, val items: List<Story>) : RecyclerView.Adapter<ItemAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): Holder {
        throw UnsupportedOperationException("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onBindViewHolder(holder: Holder?, position: Int) {
        holder?.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class Holder(view: View): RecyclerView.ViewHolder(view) {
        val by: TextView = itemView.find(R.id.id_item_by)
        val descendants: TextView = itemView.find(R.id.id_item_descendants)
        val id: TextView = itemView.find(R.id.id_item_id)
        val score: TextView = itemView.find(R.id.id_item_score)
        val time: TextView = itemView.find(R.id.id_item_time)
        val title: TextView = itemView.find(R.id.id_item_title)
        val url: TextView = itemView.find(R.id.id_item_url)

        fun bind(story: Story) {
            by.text = story.by
            descendants.text = story.descendants.toString()
            id.text = story.id.toString()
            score.text = story.score.toString()
            time.text = story.time.toString()
            title.text = story.title
            url.text = story.url
        }

    }
}