package me.ijauradunbi.heheh.ui

import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import me.ijauradunbi.heheh.R
import org.jetbrains.anko.*

class CategoryUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            textView {
                id = R.id.id_category
                singleLine = true
                ellipsize = TextUtils.TruncateAt.END
                textSize = 18f
                height = dip(40f)
                gravity = Gravity.CENTER_VERTICAL
                setPadding(dip(8f), 0, dip(8f), 0)
            }
        }
    }
}
