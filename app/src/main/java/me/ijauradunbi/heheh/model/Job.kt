package me.ijauradunbi.heheh.model

/**
 * Created on 05/09/2016.
 */
data class Job(
        val by: String,
        val id: Int,
        val score: Int,
        val text: String,
        val time: Int,
        val title: String
) {
    companion object {
        val NAME: String = "Job"
    }
}
