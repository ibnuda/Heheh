package me.ijauradunbi.heheh.util

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import me.ijauradunbi.heheh.model.*

class Deserial {
    val gson = Gson()

    // it's not fun
    fun toAsk(response: String): Ask {
        return gson.fromJson<Ask>(response)
    }

    // it's always
    fun toComment(response: String): Comment {
        return gson.fromJson<Comment>(response)
    }

    fun toJob(response: String): Job {
        return gson.fromJson<Job>(response)
    }

    fun toPoll(response: String): Poll {
        return gson.fromJson<Poll>(response)
    }

    fun toPollPart(response: String): PollPart {
        return gson.fromJson<PollPart>(response)
    }

    fun toStory(response: String): Story {
        return gson.fromJson<Story>(response)
    }

    fun toTops(response: String): List<Story> {
        return gson.fromJson<List<Story>>(response)
    }
}

