package me.ijauradunbi.heheh.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs
import me.ijauradunbi.heheh.model.*

class Netw() {
    val deserial: Deserial = Deserial()

    // TODO: For Pepe's sake, refactor this shit!!!
    fun getAsk(id: Int): Ask {
        return deserial.toAsk(getItem(id))
    }

    fun getComment(id: Int): Comment {
        return deserial.toComment(getItem(id))
    }

    fun getJob(id: Int): Job {
        return deserial.toJob(getItem(id))
    }

    fun getPollPart(id: Int): Poll {
        return deserial.toPoll(getItem(id))
    }

    fun getStory(id: Int): Story {
        return deserial.toStory(getItem(id))
    }

    fun getItem(id: Int): String {
        val url = BASE_URL + "item/" + id + ".json"
        return getJson(url)
    }

    /*
     * KINDS:
     * - top
     * - new
     * - best
     * - ask
     * - show
     * - job
     */
    fun getTops(kind: String): String {
        val url = BASE_URL + kind + "stories.json"
        return getJson(url)
    }

    fun getJson(url: String): String {
        val something: Triple<Request, Response, Result<String, FuelError>> = url.httpGet().responseString()
        val (req: Request, resp: Response, result: Result<String, FuelError>) = something
        val json: String = when(result) {
            is Result.Failure ->  """{"nothing": "nothing"}"""
            is Result.Success -> result.getAs<String>().toString()
        }
        return json
    }

    /*
    fun isOnline(): Boolean {
        val connectionManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectionManager.activeNetworkInfo
        return null != networkInfo && networkInfo.isConnected
    }
    */

    companion object {
        val BASE_URL: String = "https://hacker-news.firebaseio.com/v0/"
    }
}
