package me.ijauradunbi.heheh.presenter

import me.ijauradunbi.heheh.model.Story
import me.ijauradunbi.heheh.util.Deserial
import me.ijauradunbi.heheh.util.Netw
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.lang.kotlin.deferredObservable
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription


class ItemPresenter() {
    val subscribtion: CompositeSubscription = CompositeSubscription()
    val netw: Netw = Netw()
    val deserial: Deserial = Deserial()

    fun loadItemObservable(): Observable<MutableList<Story>> {
        return deferredObservable {
            Observable.from(deserial.toTops(netw.getTops("best")))
        }.toList().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}
